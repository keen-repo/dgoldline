<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    //return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
    return '';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);


// Billing and shipping addresses fields
add_filter( 'woocommerce_default_address_fields' , function ( $address_fields ) {
    // Only on checkout page
    if ( ! is_checkout() ) {
        return $address_fields;
    }

    // All field keys in this array
    $key_fields = array('country','first_name','last_name','company','address_1','address_2','city','state','postcode');

    // Loop through each address fields (billing and shipping)
    foreach ( $key_fields as $key_field ) {
        $address_fields[$key_field]['required'] = false;
    }

    return $address_fields;
}, 20, 1 );

add_filter( 'woocommerce_default_address_fields', function( $fields ) {
    // default priorities:
    // 'first_name' - 10
    // 'last_name' - 20
    // 'company' - 30
    // 'country' - 40
    // 'address_1' - 50
    // 'address_2' - 60
    // 'city' - 70
    // 'state' - 80
    // 'postcode' - 90

    // e.g. move 'company' above 'first_name':
    // just assign priority less than 10
    $fields['company']['priority'] = 8;
    $fields['country']['priority'] = 130;

    if ( $_GET[ 'home-delivery' ] == 1 ) {
        $fields['door_number']   = array(
            'label'        => '',
            'required'     => false,
            'class'        => array( 'form-row-wide', 'dg-door-number' ),
            'priority'     => 20,
            'placeholder'  => 'door number',
        );
    }

    /**
     * Change LABELS and insert PLACEHOLDER
     */
    // NAME
    $fields['first_name']['label'] = '';
    $fields['first_name']['placeholder'] = 'name & surname';

    $fields[ 'country' ][ 'label' ] = '';
    $fields[ 'country' ][ 'placeholder' ] = 'country';

    $fields[ 'address_1' ][ 'label' ] = '';
    $fields[ 'address_1' ][ 'placeholder' ] = 'address line 1';

    $fields[ 'address_2' ][ 'label' ] = '';
    $fields[ 'address_2' ][ 'placeholder' ] = 'address line 2';

    $fields[ 'city' ][ 'label' ] = '';
    $fields[ 'city' ][ 'placeholder' ] = 'city';

    $fields[ 'state' ][ 'label' ] = '';
    $fields[ 'state' ][ 'placeholder' ] = 'city';

    $fields[ 'postcode' ][ 'label' ] = '';
    $fields[ 'postcode' ][ 'placeholder' ] = 'postcode';

    return $fields;
} );

add_filter( 'woocommerce_shipping_fields', function( $fields ) {
    unset($fields['shipping_last_name']);
    unset($fields['shipping_company']);
    //unset($fields['shipping_city']);

    return $fields;
} );

add_filter( 'woocommerce_billing_fields', function( $fields ) {
    unset($fields['billing_last_name']);
    unset($fields['billing_company']);
    //unset($fields['billing_city']);

    /**
     * Set field order
     */
    $fields['billing_phone']['priority'] = 110;
    //$fields['billing_postcode']['priority'] = 150;

    /**
     * Change LABELS and insert PLACEHOLDER
     */
    // Email
    $fields['billing_email']['label'] = '';
    $fields['billing_email']['placeholder'] = 'email';

    // Phone
    $fields['billing_phone']['label'] = '';
    $fields['billing_phone']['placeholder'] = 'phone';

    return $fields;
}, 10, 1 );

add_filter( 'woocommerce_checkout_fields', function( $fields ) {
    unset($fields['billing']['billing_company']);// Remove Company name - always
    unset($fields['billing']['billing_shipping']);// I have no idea where this came from
    unset($fields['shipping']['shipping_last_name']);

    $fields['billing']['billing_email']['priority'] = 15;
    $fields['billing']['billing_phone']['priority'] = 18;
    $fields['billing']['billing_postcode']['priority'] = 140;

    $fields[ 'order' ][ 'order_comments' ][ 'label' ] = '';
    $fields[ 'order' ][ 'order_comments' ][ 'placeholder' ] = 'NOTE FOR DELIVERY';

    return $fields;
} );


add_action( 'woocommerce_form_field_tex', function( $field, $key ) {
    // will only execute if the field is billing_company and we are on the checkout page...
    if ( is_checkout() && ( $key == 'billing_door_number' ) ) {
        //echo '<pre>';
        //var_dump( $key );
        //var_export( $field );
        //echo '</pre>';
        $field .= '<div id="add_custom_heading"><h2>' . __('Custom Heading Here') . '</h2></div>';
    }
    return $field;
}, 10, 2 );


if ( $_GET[ 'pickup' ] == 1 ) {
    add_filter( 'woocommerce_checkout_fields' , function ( $fields ) {
        // remove billing fields
        //unset($fields['billing']['billing_first_name']);
        //unset($fields['billing']['billing_last_name']);
        unset($fields['billing']['billing_company']);
        unset($fields['billing']['billing_address_1']);
        unset($fields['billing']['billing_address_2']);
        unset($fields['billing']['billing_city']);
        unset($fields['billing']['billing_postcode']);
        unset($fields['billing']['billing_country']);
        unset($fields['billing']['billing_state']);
        //unset($fields['billing']['billing_phone']);
        //unset($fields['billing']['billing_email']);

        // remove shipping fields
        unset($fields['shipping']['shipping_first_name']);
        unset($fields['shipping']['shipping_last_name']);
        unset($fields['shipping']['shipping_company']);
        unset($fields['shipping']['shipping_address_1']);
        unset($fields['shipping']['shipping_address_2']);
        unset($fields['shipping']['shipping_city']);
        unset($fields['shipping']['shipping_postcode']);
        unset($fields['shipping']['shipping_country']);
        unset($fields['shipping']['shipping_state']);

        // remove order comment fields
        unset($fields['order']['order_comments']);

        return $fields;
    } );
}

add_filter( 'woocommerce_cart_shipping_method_full_label', function( $label, $method ) {
    $new_label = preg_replace( '/^.+:/', '', $label );
    return $new_label;
}, 10, 2 );


add_filter('woocommerce_catalog_orderby', function( $sorting_options ) {
    $sorting_options = array(
        'menu_order' => __( 'Default sorting', 'woocommerce' ),
        'popularity' => __( 'Popularity', 'woocommerce' ),
        'rating'     => __( 'Average rating', 'woocommerce' ),
        'date'       => __( 'Latest', 'woocommerce' ),
        'price'      => __( 'Price: low to high', 'woocommerce' ),
        'price-desc' => __( 'Price: high to low', 'woocommerce' ),
    );

    return $sorting_options;
}, 10, 2 );


