<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function getCustomLogo()
    {
        if (get_custom_logo()) {
            return get_custom_logo();
        }
    }

    public function getsocials()
    {
        return get_fields( 'options' )[ 'social_icons' ][ 'social_icons' ]; // twice because we have a Repeater in a Clone field
    }

    public function shopmessage()
    {
        return get_fields( 'options' )[ 'site_wide_shop_message' ]; // twice because we're in Clone field
    }

    public static function login_register_menu()
    {
        if ( ! is_admin(  ) ) {
            global $woocommerce;
            $cart_url = $woocommerce->cart->get_cart_url();
            $data = [];
            $data[ 'loginout' ]     = wp_loginout( false, false );
            $data[ 'register' ]     = wp_registration_url();
            $data[ 'page_links' ]   = get_fields( 'options' )[ 'page_links' ];
            $data[ 'cart_url' ]     = $cart_url;
            return $data;
        }
    }

    public function get_shop_menu_object()
    {
        $menu_name  = 'shop_navigation';
        $locations  = get_nav_menu_locations();
        $menu_id    = $locations[ $menu_name ];
        return wp_get_nav_menu_items( $menu_id );
    }
}
