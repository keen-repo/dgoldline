@php
/**
 * Template Name: Delivery Options
 */

global $woocommerce;
$checkoutURL = $woocommerce->cart->get_checkout_url();

@endphp

@extends('layouts.app')

@section( 'page-header' )
    <div class="page-header">
      <h1>Checkout</h1>
    </div>
@endsection

@section('content')
  <div class="dg-delivery-options-title">
    <span class="title">SELECT OPTION</span>
  </div>
  <div class="dg-delivery-options">
    <a href="{{ $checkoutURL }}?home-delivery=1" class="delivery-home">
      <div class="">
      <img src="@asset( 'images/icons/delivery-truck.png' )" alt="" class="icon">
      <div class="title">HOME DELIVERY</div>
      </div>
    </a>
    <a href="{{ $checkoutURL }}?pickup=1" class="delivery-store">
      <div class="">
        <img src="@asset( 'images/icons/store.png' )" alt="" class="icon">
        <div class="title">STORE COLLECTION</div>
      </div>
    </a>
  </div>
@endsection
