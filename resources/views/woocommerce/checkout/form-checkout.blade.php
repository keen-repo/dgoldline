@php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}
@endphp

{{--
<div class="dg-delivery-options">
  <a href="?home-delivery=1" class="delivery-home @php echo $_GET[ 'home-delivery' ] == 1 ? 'homedel' : ''; @endphp">
    <div class="">
    <img src="@asset( '/images/icons/delivery-truck.png' )" alt="" class="icon">
    <div class="title">HOME DELIVERY</div>
    </div>
  </a>
  <a href="?pickup=1" class="delivery-store @php echo $_GET[ 'pickup' ] == 1 ? 'pickup' : ''; @endphp">
    <div class="">
      <img src="@asset( '/images/icons/store.png' )" alt="" class="icon">
      <div class="title">STORE COLLECTION</div>
    </div>
  </a>
</div>--}}

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="@php
echo esc_url( wc_get_checkout_url() ); @endphp" enctype="multipart/form-data">

	@php
  if ( $checkout->get_checkout_fields() ) : @endphp

		@php
    do_action( 'woocommerce_checkout_before_customer_details' );
    @endphp
    @php
    if ( $_GET[ 'pickup' ] == 1 ) {
      WC()->session->set('chosen_shipping_methods', array( 'local_pickup:10' ) );

      $showField = ' show_fields pickup';
    } elseif ( $_GET[ 'home-delivery' ] == 1 ) {
      $showField = ' show_fields home-delivery';
    }
    @endphp
		<div class="col2-set{{ $showField }}" id="customer_details">
			<div class="col-1">
				@php
        do_action( 'woocommerce_checkout_billing' ); @endphp
			</div>

			<div class="col-2">
				@php
        do_action( 'woocommerce_checkout_shipping' ); @endphp
        @if ( $_GET[ 'pickup' ] == 1 )
        @include ( 'shop/opening-hours' )
        @endif
			</div>

      <div class="dg-checkout-navigation">
        @php
        $btnTitle = 'next';
        $btnLink = '#';
        $classes = 'next';
        @endphp
        @include ( 'partials/buttons/primary-btn' )
      </div>
		</div>

  	@php
    endif; @endphp

		@php
    do_action( 'woocommerce_checkout_after_customer_details' );
    @endphp

  <div id="dg-order-review">
    <div class="dg-user-account-information">
      <div class="side-left">
        <h3 class="billing-info">Personal Information</h3>
        <div class="billing-info-name billing_first_name"></div>
        <div class="billing-info-email billing_email"></div>
        <div class="billing-info-phone billing_phone"></div>
        <h3 class="delivery-address">Delivery Address</h3>
        <div class="delivery-address-doornum billing_door_number"></div>
        <div class="delivery-address-1 billing_address_1"></div>
        <div class="delivery-address-2 billing_address_2"></div>
        <div class="delivery-address-city billing_state"></div>
        <div class="delivery-address-postcode billing_postcode"></div>
      </div>
      <div class="side-right">
        <h3 class="receiver-info">Receiver Information</h3>
        <div class="receiver-info-name"></div>
        <div class="receiver-info-phone"></div>
        <div class="shipping_first_name"></div>
        <div class="shipping_door_number"></div>
        <div class="shipping_address_1"></div>
        <div class="shipping_address_2"></div>
        <div class="shipping_state"></div>
        <div class="shipping_postcode"></div>
        <h3 class="delivery-type">Type</h3>
        <div class="delivery-type-type">
          @if ( $_GET[ 'home-delivery' ] == 1 )
          Home Delivery
          @elseif ( $_GET[ 'pickup' ] == 1 )
          Store Collection
          @endif
        </div>
      </div>
    </div>

    <div class="dg-order-details">
    	@php
      do_action( 'woocommerce_checkout_before_order_review_heading' ); @endphp

    	<h3 id="order_review_heading">@php
        esc_html_e( 'order details', 'woocommerce' ); @endphp</h3>

    	@php
      do_action( 'woocommerce_checkout_before_order_review' ); @endphp

    	<div id="order_review" class="woocommerce-checkout-review-order">
    		@php
        do_action( 'woocommerce_checkout_order_review' ); @endphp
    	</div>

    	@php
      do_action( 'woocommerce_checkout_after_order_review' ); @endphp

      @php
      add_action( 'woocommerce_after_checkout_form', 'woocommerce_checkout_coupon_form', 15 ); @endphp
    </div>

    <div class="dg-checkout-navigation">
      @php
      $btnTitle = 'prev';
      $btnLink = '#';
      $classes = 'prev';
      @endphp
      @include ( 'partials/buttons/primary-btn' )
    </div>
  </div>
</form>

@php
do_action( 'woocommerce_after_checkout_form', $checkout ); @endphp
