{{--
The template for displaying product content within loops
This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.
--}}

@php
defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
    return;
}
@endphp

<li @php wc_product_class( '', $product ); @endphp>
    @php
    do_action( 'woocommerce_before_shop_loop_item' );
    @endphp

    <div class="product-image">
      @php
      do_action( 'woocommerce_before_shop_loop_item_title' );
      @endphp
      <div class="product-image-overlay"><span>buy now</span></div>
    </div>

    <div class="sku-product">
      <div class="wrapper">
        <span class="sku-title">CODE:</span>
        <div class="sku-code">@php echo $product->get_sku(); @endphp
          @php
          echo do_shortcode( '[ti_wishlists_addtowishlist]' );
          //echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
          @endphp
        </div>
      </div>
    </div>
    @php
    do_action( 'woocommerce_shop_loop_item_title' );

    do_action( 'woocommerce_after_shop_loop_item_title' );

    do_action( 'woocommerce_after_shop_loop_item' );
    @endphp
</li>
