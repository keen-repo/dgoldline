{{--
The Template for displaying product archives, including the main shop page which is a post type archive
This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.
@see https://docs.woocommerce.com/document/template-structure/
@package WooCommerce/Templates
@version 3.4.0
--}}

@extends('layouts.shop')

@section( 'shop-header' )
  <header class="woocommerce-products-header">
    @if(apply_filters('woocommerce_show_page_title', true))
      <h1 class="woocommerce-products-header__title page-title">{!! woocommerce_page_title(false) !!}</h1>
      @php
      woocommerce_breadcrumb(  );
      @endphp
    @endif

  </header>
@endsection

@section('content')
  @php
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
    do_action('get_header', 'shop');
    do_action('woocommerce_before_main_content');
  @endphp


  @if(woocommerce_product_loop())
    @php
      do_action('woocommerce_before_shop_loop');
      woocommerce_product_loop_start();
    @endphp

    @if(wc_get_loop_prop('total'))
      @while(have_posts())
        @php
          the_post();
          do_action('woocommerce_shop_loop');
          wc_get_template_part('content', 'product');
        @endphp
      @endwhile
    @endif

    @php
      woocommerce_product_loop_end();
      do_action('woocommerce_after_shop_loop');
    @endphp
  @else
    @php
      do_action('woocommerce_no_products_found');
    @endphp
  @endif

  @php
    do_action('woocommerce_after_main_content');
  @endphp

  <aside class="shop-sidebar">
    @php
      dynamic_sidebar('sidebar-primary');
    @endphp
  </aside>

  @php
    //do_action('get_sidebar', 'shop');
    do_action('get_footer', 'shop');
  @endphp

  @if ( is_product_category(  ) )
    @php
    do_action('woocommerce_archive_description');
    @endphp
  @endif

  @php
  $shopPage = get_option( 'woocommerce_shop_page_id' );
  $bottomText = get_field( 'bottom_text', $shopPage );

  $filter_cat_id = $_GET["filter_cat_0"];
  $filtered_cat = get_term_by("id", $filter_cat_id, "product_cat");
  $bottomText = $filtered_cat->description;
  @endphp
  @if ( is_shop(  ) )
    <div class="shop-page-message">
      <div class="message">{!! $bottomText !!}</div>
    </div>
  @endif

@endsection
