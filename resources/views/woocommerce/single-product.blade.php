{{--
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
--}}
@php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
//get_header( 'shop' );
@endphp

@extends( 'layouts.shop' )

@section( 'shop-header' )
  <header class="woocommerce-products-header">
    @php
      woocommerce_breadcrumb(  );
    @endphp
  </header>
@endsection

@section( 'content' )

@php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
@endphp

		@php
    while ( have_posts() ) : the_post();

			wc_get_template_part( 'content', 'single-product' );

		endwhile; // end of the loop.
    @endphp

	@php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	@endphp

	@php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	@endphp

  @php
  //get_footer( 'shop' );
  @endphp

  @php
  $rP = get_fields(  );
  $titleBold = $rP[ 'title_bold' ];
  $titleRegular = $rP[ 'title_regular' ];
  $titleText = $rP[ 'subtitle_text' ];
  $prods = $rP[ 'select_products' ];
  $prodIDArray = [  ];
  @endphp

  @if ( $prods )
    <div class="ymaw-products">
      @include ( 'partials/blocks/top-title' )
      @foreach ( $prods as $fld )
        @php
        $prodID = $fld->ID;
        array_push( $prodIDArray, $prodID );
        @endphp
      @endforeach
      @php
        $prodIDString = implode( ', ', $prodIDArray );
        if ( ! is_admin(  ) ) {
          echo do_shortcode( "[products ids='{$prodIDString}' columns=4]" );
        }
      @endphp
    </div>
  @endif
@endsection
