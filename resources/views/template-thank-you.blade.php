@php
/**
 * Template Name: Thank You
 */

global $woocommerce;
$checkoutURL = $woocommerce->cart->get_checkout_url();

@endphp

@extends('layouts.app')

@section( 'page-header' )
    <div class="page-header">
      <h1>THANK YOU!</h1>
      <p>Your order has been placed.</p>
      <p>Our team will check that payment has been received and proceed accordingly.</p>
    </div>
@endsection
