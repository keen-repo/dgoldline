{{--
  Title: Simple Image Slider
  Description: Simple image slider. Extends full page width.
  Category: dg_block_category
  Icon: admin-comments
  Keywords: image, slider, full width
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

$images = $flds[ 'images' ];

if ( ! $active ) {
  return;
}
@endphp

<section data-{{ $block[ 'id' ] }} class="{{ $block[ 'classes' ] }} @php echo is_front_page() ? '' : 'dg-full-width-block'; @endphp">
  <div class="simple-slider-wrapper">
    <div class="simple-slider-slides">
      @foreach ( $images as $image )
        @php
        $img = aq_resize( $image[ 'image' ][ 'url' ], 378, 378, true );
        @endphp
        <div class="simple-slide">
          <img src="{{ $img }}" alt="">
        </div>
      @endforeach
    </div>
    <div class="overlay"></div>
  </div>
</section>
