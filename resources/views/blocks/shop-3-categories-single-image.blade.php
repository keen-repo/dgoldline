{{--
  Title: (Shop) 3 Categories Single Image
  Description: test
  Category: dg_block_category
  Icon: admin-comments
  Keywords: shop, category, categories
--}}

<section data-{{ $block[ 'id' ] }} class="{{ $block[ 'classes' ] }} @php echo is_front_page() ? '' : 'dg-full-width-block'; @endphp">
    @php
    $flds  = get_fields(  );
    $cats = $flds[ 'select_shop_categories' ];
    $image = $flds[ 'background_image' ][ 'url' ];
    @endphp
    <div class="shop-categories" style="background-image: url('{{ $image  }}')">
      @foreach ( $cats as $fld )
        @php
        $title = $fld->name;
        $termID = $fld->term_taxonomy_id;
        $link = get_term_link( $termID, 'product_cat' );
        @endphp
        <a href="{{ $link }}" rel="bookmark" class="shop-category">
          <div class="category-content shop-category-{{ $loop->iteration }}">
            <div class="title"><strong>Shop</strong> {{ $title }}</div>
          </div>
        </a>
      @endforeach
    </div>
</section>
{{--
<style type="text/css">
  [data-{{$block['id']}}] {
    background: {{get_field('color')}};
  }
</style>
--}}
