{{--
  Title: Bulleted Options
  Description: Accordion bulleted options
  Category: dg_block_category
  Icon: admin-comments
  Keywords: bullet, options, accordion
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

$opts = $flds[ 'options' ];

if ( ! $active ) {
  return;
}
@endphp

<section data-{{ $block[ 'id' ] }} class="{{ $block[ 'classes' ] }}">
  <div class="bulleted-options-wrapper">
    @foreach ( $opts as $opt )
      <div class="bulleted-option">
        <div class="bulleted-option-title"><img src="@asset('images/icons/arrow-right.png')">{{ $opt[ 'title' ] }}</div>
        <div class="bulleted-option-text">{!! $opt[ 'text' ] !!}</div>
      </div>
    @endforeach
  </div>
</section>
