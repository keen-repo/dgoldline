{{--
  Title: Grey Block
  Description: Block with grey background
  Category: dg_block_category
  Icon: admin-comments
  Keywords: grey, background
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

$images = $flds[ 'images' ];

if ( ! $active ) {
  return;
}
@endphp

<section data-{{ $block[ 'id' ] }} class="{{ $block[ 'classes' ] }} dg-full-width-block">
  <div class="grey-background-wrapper">
    <div class="grey-background-text">
      {!! $flds[ 'text' ] !!}
    </div>
  </div>
</section>
