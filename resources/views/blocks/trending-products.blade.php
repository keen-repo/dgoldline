{{--
  Title: Trending Products
  Description: test
  Category: dg_block_category
  Icon: admin-comments
  Keywords: trending, products
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];
$trendy = $flds[ 'select_trending_products' ];

$titleBold = $flds[ 'title_bold' ];
$titleRegular = $flds[ 'title_regular' ];
$text = $flds[ 'text' ];

$prodIDArray = [  ];

if ( ! $active ) {
  return;
}
@endphp

<section data-{{ $block[ 'id' ] }} class="{{ $block[ 'classes' ] }}">
  <div class="section-title">
    <span class="bold">{{ $titleBold }}</span>
    <span class="regular">{{ $titleRegular }}</span>
  </div>
  @if ( $text )
  <div class="section-text">{{ $text }}</div>
  @endif
  @foreach ( $trendy as $fld )
    @php
    $prodID = $fld->ID;
    array_push( $prodIDArray, $prodID );
    @endphp
  @endforeach

  @php
    $prodIDString = implode( ', ', $prodIDArray );
    if ( ! is_admin(  ) ) {
      echo do_shortcode( "[products ids='{$prodIDString}' columns=4 orderby='title']" );
    }
  @endphp
  @php
  $btnLink = get_permalink( wc_get_page_id( 'shop' ) );
  $btnTitle = 'SHOP ALL PRODUCTS';
  @endphp
  <div class="shop-page-btn">
    <div class="border-left"></div>
    @include ( 'partials/buttons/primary-btn' )
    <div class="border-right"></div>
  </div>
</section>
