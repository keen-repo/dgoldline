{{--
  Title: Image Text / Text Right
  Description: test
  Category: dg_block_category
  Icon: admin-comments
  Keywords: image, text, left, right
--}}

<section data-{{$block['id']}} class="{{$block['classes']}}">
    @php
    $flds  = get_fields(  );
    // Image Side ( Left )
    $image          = $flds[ 'image' ][ 'url' ];
    $img            = aq_resize( $image, 597, 620, true );
    $titleBold      = $flds[ 'title_bold' ];
    $titleRegular   = $flds[ 'title_regular' ];
    $titleText      = $flds[ 'title_text' ];
    // Text Side ( Right )
    $title          = $flds[ 'title' ];
    $text           = $flds[ 'text' ];

    $link           = $flds[ 'link' ];
    $btnTitle       = $link[ 'title' ];
    $btnLink        = $link[ 'url' ];
    $btnTarget      = $link[ 'target' ];
    @endphp

    @include ( 'partials/blocks/top-title' )

    <div class="image-content">
      <div class="side-left">
        <div class="image">
          <img src="{{ $img }}" alt="">
        </div>
      </div>
      <div class="side-right">
        <div class="title-content">
          <h3 class="title">{{ $title }}</h3>
          @include ( 'partials/maltese-cross' )
          <div class="content">{{ $text }}</div>
          @include ( 'partials/buttons/primary-btn' )
        </div>
      </div>
    </div>
</section>
