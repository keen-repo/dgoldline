{{--
  Title: (Shop) 3 Categories Images
  Description: test
  Category: dg_block_category
  Icon: admin-comments
  Keywords: shop, category, categories
--}}

<section data-{{ $block[ 'id' ] }} class="{{ $block[ 'classes' ] }} @php echo is_front_page() ? '' : 'dg-full-width-block'; @endphp">
    @php
    $flds  = get_fields(  );
    $titleBold      = $flds[ 'title_bold' ];
    $titleRegular   = $flds[ 'title_regular' ];
    $titleText      = $flds[ 'title_text' ];
    $cats = $flds[ 'product_categories' ];
    @endphp
    @include ( 'partials/blocks/top-title' )
    <div class="shop-categories shop-category-slides">
      @foreach ( $cats as $fld )
        @php
        $image = $fld[ 'background_image' ][ 'url' ];
        $title = $fld[ 'product_category' ]->name;
        $termID = $fld[ 'product_category' ]->term_taxonomy_id;
        $link = get_term_link( $termID, 'product_cat' );
        @endphp
        <div class="shop-category-slide">
          <a href="{{ $link }}" rel="bookmark" class="shop-category">
            <div class="category-content shop-category-{{ $loop->iteration }}">
              <div class="title">{{ $title }}</div>
              <img src="{{ $image  }}" alt="">
            </div>
          </a>
        </div>
      @endforeach
    </div>
</section>
{{--
<style type="text/css">
  [data-{{$block['id']}}] {
    background: {{get_field('color')}};
  }
</style>
--}}
