{{--
  Title: Hero Image Slider
  Description: test
  Category: dg_block_category
  Icon: admin-comments
  Keywords: testimonial quote
--}}

<section data-{{ $block[ 'id' ] }} class="{{ $block[ 'classes' ] }}">
    @php
    $allFields  = get_fields(  );
    $flds       = $allFields[ 'hero_images' ];
    @endphp
    <div class="hero-slider-image" style="background-image: url('{{ $allFields[ 'image' ][ 'sizes' ][ '1920x1080' ] }}');"></div>
    <div class="hero-slides">
      @foreach ( $flds as $fld )
        <div class="hero-slider">
          <div class="hero-slide-wrap">
            <div class="slide-wrapper">
              @php
                $image       = $fld[ 'image' ];
                $title       = $fld[ 'title' ];
                $text        = $fld[ 'text' ];
                $btn         = $fld[ 'button' ];
                $btnTitle    = $btn[ 'title' ];
                $btnLink     = $btn[ 'url' ];
                $btnTarget   = $btn[ 'target' ];
                $btnTitle    = 'discover more';
              @endphp
              <div class="image">
                <img src="{{ $image[ 'sizes' ][ '674x674' ] }}" alt="{{ $image[ 'alt' ] }}">
              </div>
              <div class="slide-content">
                <div class="content-box">
                  @if ( $title )
                  <h2 class="title">{{ $title }}</h2>
                  @include ( 'partials/maltese-cross' )
                  @endif
                  @if ( $text )
                  <div class="text">{{ $text }}</div>
                  @endif
                  @include ( 'partials/buttons/primary-btn' )
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
</section>
{{--
<style type="text/css">
  [data-{{$block['id']}}] {
    background: {{get_field('color')}};
  }
</style>
--}}
