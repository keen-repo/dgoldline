{{--
  Title: Testimonials
  Description: test
  Category: dg_block_category
  Icon: admin-comments
  Keywords: image, text, left, right
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

$image = $flds[ 'image' ][ 'url' ];
$img = aq_resize( $image, 1920, 498, true );

$titleBold = $flds[ 'title_bold' ];
$titleRegular = $flds[ 'title_regular' ];

$testimonials = $flds[ 'testimonials' ];

if ( ! $active ) {
  return;
}
@endphp

<section data-{{$block['id']}} class="{{$block['classes']}}">
  <div class="testimonial-wrapper" style="background-image: url( '{{ $img }}' );">
    @include ( 'partials/blocks/top-title' )
    <div class="testimonials-slides">
      @foreach ( $testimonials as $testimonial )
        <div class="testimonial">
          <div class="testimony">{{ $testimonial[ 'testimony' ] }}</div>
          <div class="name">{{ $testimonial[ 'name' ] }}</div>
        </div>
      @endforeach
    </div>
    <div class="overlay"></div>
  </div>
</section>
