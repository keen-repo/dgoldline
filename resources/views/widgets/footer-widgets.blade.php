<section class="widget footer_address">
<h3 class="widget-title">
  {!! App::getCustomLogo() !!}
</h3>

@php
$blackSocials = false;
@endphp

<div class="widget-body">
  @include ( 'partials/contact-details' )
</div>

</section>
@php dynamic_sidebar('sidebar-footer') @endphp
