<div class="footer-newsletter">
  <div class="newsletter-content">
    @php
    $titleText = '';
    @endphp
    @include ( 'partials/blocks/top-title' )
    <!-- Begin Mailchimp Signup Form -->
    <div id="mc_embed_signup">
        <form action="https://moodexpress.us18.list-manage.com/subscribe/post?u=dfd5b57c6548e79a180c457d8&id=8e3727cc29" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
            <div id="mc_embed_signup_scroll">
                <div class="mc-field-group">
                    <label class="email-label" for="mce-EMAIL">
                        Email Address
                    </label>
                    <input class="required email" id="mce-EMAIL" name="EMAIL" type="email" value="" placeholder="Email Address">
                    </input>
                </div>
                <div class="clear" id="mce-responses">
                    <div class="response" id="mce-error-response" style="display:none">
                    </div>
                    <div class="response" id="mce-success-response" style="display:none">
                    </div>
                </div>
                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div aria-hidden="true" style="position: absolute; left: -5000px;">
                    <input name="b_dfd5b57c6548e79a180c457d8_8e3727cc29" tabindex="-1" type="text" value=""/>
                </div>
                <div class="clear">
                    <input class="button subscribe-btn btn btn-primary" id="mc-embedded-subscribe" name="subscribe" type="submit" value="Subscribe Now"/>
                </div>
            </div>
        </form>
    </div>
    <!--End mc_embed_signup-->
  </div>
</div>
