@php
$flds   = get_fields( 'options' );
$pM     = $flds[ 'payment_methods' ];
$title  = $pM[ 'title' ];
$icons  = $pM[ 'payment_methods' ];
@endphp
<hr>
<div class="payment-credits">
  <div class="pc-wrapper">
    <div class="payment-methods">
      <div class="wrapper">
        <div class="title">{{ $title }}</div>
        <div class="images">
          @foreach ( $icons as $icon )
            @php
            $iconURL = $icon[ 'icon' ][ 'url' ];
            $img = aq_resize( $iconURL, 39, 27, false );
            @endphp
            <div class="image">
              <img src="{{ $img }}">
            </div>
          @endforeach
        </div>
      </div>
    </div>

    <div class="credits">
      <div class="credit copyright">&copy; {{ date( 'Y' ) }} {{ get_bloginfo( 'name', 'display' ) }}. All rights reserved.</div>
      <span class="sep">|</span>
      <div class="credit privacy-policy"><a href="{{ $flds[ 'page_links' ][ 'privacy_policy' ] }}">Privacy Policy</a></div>
      <span class="sep">|</span>
      <div class="credit design"><a href="https://keen.com.mt">Designed and Developed by Keen Malta</a></div>
    </div>
  </div>
</div>
