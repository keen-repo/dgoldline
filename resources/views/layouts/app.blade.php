<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap container" role="document">
      @if ( !is_front_page() )
        <div class="custom-page-header">
          @yield( 'page-header' )
          @php
          // Do not output Breadcrumbs if
          // we're on the Thank You page
          $classToCheck = 'template-thank-you';
          $bodyClasses = get_body_class(  );
          if ( !in_array( $classToCheck, $bodyClasses ) ) {
            woocommerce_breadcrumb(  );
          }
          @endphp
        </div>
      @endif
      <div class="content">
        <main class="main">
          <!-- APP -->
          @yield('content')
        </main>
        @if (App\display_sidebar())
          <aside class="sidebar">
            @include('partials.sidebar')
          </aside>
        @endif
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
