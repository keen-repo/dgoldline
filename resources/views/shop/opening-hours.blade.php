@php
$flds = get_fields( 'options' )[ 'opening_hours' ];
$title = $flds[ 'title' ];
$note = $flds[ 'note' ];
$oH = $flds[ 'opening_hours' ];
@endphp

<div class="opening-hours">
  <div class="title">{{ $title }}</div>

  @foreach ( $oH as $hours )
    @php
    $days = $hours[ 'days' ];
    $open = $hours[ 'opening_time' ];
    $close = $hours[ 'closing_time' ];
    @endphp
    <div class="opening-day">
      <div class="days">{{ $days }}</div>
      <span class="open">{{ $open }}</span> - <span class="close">{{ $close }}</span>
    </div>
  @endforeach

  <div class="note">{{ $note }}</div>
</div>
