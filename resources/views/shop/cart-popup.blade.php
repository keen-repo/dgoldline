@php
global $woocommerce;
$items = $woocommerce->cart->get_cart();
$checkoutURL = $woocommerce->cart->get_checkout_url();
$checkoutURL = get_fields( 'options' )[ 'page_links' ][ 'checkout_delivery_options' ];

$totalQtys = $woocommerce->cart->get_cart_contents_count(  );
if ( $totalQtys == 1 ) {
  $itemText = 'ITEM';
} else {
  $itemText = 'ITEMS';
}


// shop page link
$btnLink = get_permalink( wc_get_page_id( 'shop' ) );
$btnLinkCart = get_permalink( wc_get_page_id( 'cart' ) );
@endphp

<div class="dg-cart-popup">
  <div class="total-quantities">
    <strong>MY BAG</strong>
    <span class="quantities">{{ $totalQtys }} {{ $itemText }}</span>
  </div>
  <div class="dg-cart-products">
    @php
    foreach( $items as $item => $values ) {
      $_product =  wc_get_product( $values[ 'data' ]->get_id() );
      $price = get_post_meta( $values[ 'product_id' ] , '_price', true );
      $currency = get_woocommerce_currency_symbol(  );
      $productDetail = wc_get_product( $values[ 'product_id' ] );
      $subTotal = $woocommerce->cart->get_cart_subtotal();
      $productID = $productDetail->get_id(  );
      @endphp
      <div class="dg-cart-product">
        {{-- Image --}}
        <div class="dg-cart-image">
        {!! $productDetail->get_image( 'woocommerce_thumbnail' ) !!}
        </div>
        <div class="dg-cart-content">
          {{-- Title --}}
          <div class="title"><b>{{ $_product->get_title() }}</b></div>
          {{-- Description --}}
          <div class="desc">{{ str_limit( $_product->get_description(), 80 ) }}</div>
          {{-- Quantity
          Quantity: {{ $values[ 'quantity' ] }}
          --}}
          {{-- Price --}}
          <div class="price">{!! $currency !!}{{ $price }}</div>
          {{--<a href="{{ $woocommerce->cart->get_remove_url( $productID ) }}">remove</a>--}}
        </div>
        {{-- Subtotal --}}
      </div>
      @php
    }
    @endphp
  </div>
  <div class="dg-cart-subtotal">
    <span class="title">Sub-Total</span>
    <span class="subtotal-amount">{!! $subTotal !!}</span>
  </div>
  <div class="dg-cart-buttons">
    <!-- View Cart button -->
    <a href="{{ $btnLinkCart }}" class="btn btn-outline view-cart">view cart</a>
    <!-- Checkout button -->
    @if ( is_user_logged_in() )
      <a href="{{ $checkoutURL }}" class="btn btn-primary checkout-btn">checkout</a>
    @else
      <a href="#" class="btn btn-primary checkout-btn logged-out">checkout</a>
    @endif
    <!-- Continue Shopping button -->
    <div class="dg-continue-shopping-wrap">
      <a href="{{ $btnLink }}" class="btn btn-outline close">continue shopping</a>
    </div>
  </div>
</div>
