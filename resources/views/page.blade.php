@extends( is_checkout() ? 'layouts.shop' : 'layouts.app')

@section( 'page-header' )
  @include('partials.page-header')
@endsection

@section('content')
  @while(have_posts()) @php the_post() @endphp

    @if ( is_checkout() )
      @section( 'shop-header' )
        @include('partials.page-header')
        <header class="woocommerce-products-header">
          @php
            woocommerce_breadcrumb(  );
          @endphp
        </header>
      @endsection
    @endif

    @include('partials.content-page')

  @endwhile
@endsection
