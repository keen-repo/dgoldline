@extends( is_checkout() ? 'layouts.shop' : 'layouts.app')

@section( 'page-header' )
  @include('partials.page-header')
@endsection

@section('content')
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @php
    global $paged;
    if ( $paged == '' ) {
      $paged = 1;
    }
    if ( $paged == 1 ) {
      $firstPost = 0;
    } elseif ( $paged > 1 ) {
      $firstPost = $paged;
    }
  @endphp

  @while (have_posts()) @php the_post() @endphp
    @php
    if ( $firstPost == 0 ) {
      $firstPostClass = 'dg-first-post';
    } else {
      $firstPostClass = 'dg-other-posts';
    }
    @endphp

    @include( 'partials.content-' . get_post_type(), [ 'class' => $firstPostClass ] )

    @php
    $firstPost++;
    @endphp
  @endwhile

  {!! dg_custom_pagination() !!}
@endsection
