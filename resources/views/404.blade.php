@extends('layouts.404')

@section('content')
  {{--@include('partials.page-header')--}}

  @if (!have_posts())
    <div class="container">
      <div class="image"><img src="@asset( 'images/404.png' )" alt=""></div>
      <div class="page-title">Oops...</div>
      <div class="message">The webpage you are looking for is not here!</div>
      <a href="{{ get_home_url( '/' ) }}" class="btn btn-primary">go to homepage</a>
    </div>
  @endif
@endsection
