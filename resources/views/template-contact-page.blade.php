@php
/**
 * Template Name: Contact Page
 */

global $woocommerce;
$checkoutURL = $woocommerce->cart->get_checkout_url();

@endphp

@extends('layouts.app')

@section( 'page-header' )
  @include('partials.page-header')
@endsection


@section('content')
  @php
  $flds = get_fields( 'options' )[ 'contact_numbers' ];
  $blackSocials = true;
  @endphp
  <section class="contact-section">
    <div class="contact-wrapper">
      <div class="content_address">
        <h3>Send us a message</h3>
        @include ( 'partials/contact-details' )
      </div>
      <div class="dg-contact_form">
        @php
        $formID = get_field( 'contact_form_id' );
        echo do_shortcode( '[formidable id='.$formID.']' );
        @endphp
      </div>
    </div>

    @php
    $lat = $flds[ 'map_coords' ][ 'lat' ];
    $lng = $flds[ 'map_coords' ][ 'lng' ];
    @endphp

    @if ( $lat and $lng )
      <div class="contact-map">
        <div id="mapid" style="height:400px;"></div>
      </div>
      <script>
        jQuery( document ).ready( function(  ) {
          var map = L.map( 'mapid' ).setView( [ {{ $lat }}, {{ $lng }} ], 16 );
          var mapLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>';

          L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              attribution: '&copy; ' + mapLink + ' Contributors',
              //minZoom: 11,
              //maxZoom: 11,
            }).addTo(map);

          var agIcon = L.icon({
            iconUrl: "@asset( 'images/icons/map-marker.png' )",
            iconSize: [40,42],
            iconAnchor: [20,42]
          });

          marker = new L.marker( [ {{ $lat }}, {{ $lng }} ], {icon: agIcon} ).addTo( map );
        } );
      </script>
    @endif
  </section>
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-page')
  @endwhile
@endsection
