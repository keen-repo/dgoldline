<nav id="site-navigation" class="main-navigation nav-primary">
  <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="sr-only">Primary Menu</span></button>
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav list-unstyled']) !!}
  @endif
</nav>
