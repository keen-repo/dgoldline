@if ( get_post_type() === 'post' )
  @include('partials/content')

@elseif ( get_post_type() === 'product' )
  <div class="woocommerce columns-3">
    <ul class="products columns-3">
      @include('woocommerce/content-product')
    </ul>
  </div>
@endif
