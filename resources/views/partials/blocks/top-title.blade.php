<div class="top-title">
  <h2 class="mixed-title">
    <span class="bold">{{ $titleBold }}</span>
    <span class="regular">{{ $titleRegular }}</span>
  </h2>
  @if ( $titleText )
  <div class="title-text">{{ $titleText }}</div>
  @endif
</div>
