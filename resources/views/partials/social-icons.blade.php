@if ( $getsocials )
  <nav class="social-icons">
    <ul class="list-unstyled">
      @foreach ($getsocials as $social)
        <li class="social-icon {{ $loop->last ? 'last-item' : '' }}">
          @if ( $blackSocials )
          <a rel="external" href="{{ $social[ 'link' ] }}"><img src="@asset('images/icons/' . $social['title'] . '_black.png')" alt="{{ $social[ 'title' ] }} icon"></a>
          @else
          <a rel="external" href="{{ $social[ 'link' ] }}"><img src="@asset('images/icons/' . $social['title'] . '.png')" alt="{{ $social[ 'title' ] }} icon"></a>
          @endif
        </li>
      @endforeach
      @if ( $showSearch )
        <li class="search-icon"><img src="@asset('images/icons/Search.png')" alt=""></li>
      @endif
    </ul>
  </nav>
@endif
