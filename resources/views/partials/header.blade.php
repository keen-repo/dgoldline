@include( 'partials.preloader' )
<header class="banner">
  <div class="container">
    <a class="grant-header-mobile" href="https://businessenhance.gov.mt/" target="_blank"><img src="@asset("images/grant-header.png")" alt=""></a>
    <div class="branding-wrapper">
      @php
      $showSearch = true;
      $blackSocials = false;
      @endphp
      <div class="shop-menu-open"><span class="dashicons dashicons-menu-alt3"></span></div>
      @include( 'partials.social-icons' )
      <a class="grant-header" href="https://businessenhance.gov.mt/" target="_blank"><img src="@asset("images/grant-header.png")" alt=""></a>
      {!! App::getCustomLogo() !!}
      <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
      {{--@include( 'partials.main-menu' )--}}
      @include( 'partials.login-register-menu' )
    </div>
  </div>
  @include( 'partials.site-wide-shop-message' )
</header>
@if ( ! is_404() )
@include( 'partials.shop-menu' )
@endif
