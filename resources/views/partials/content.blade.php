<article @php post_class( $class ) @endphp>
  @php
  $url = get_the_post_thumbnail_url();
  if ( $firstPost == 0 ) {
    $img = aq_resize( $url, 597, 597, true );
  } else {
    $img = aq_resize( $url, 378, 378, true );
  }
  @endphp
  <div class="post-content-wrapper">
    <div class="post-thumbnail">
      @if ( has_post_thumbnail() )
        <img src="{{ $img }}" alt="">
      @else
        <img src="@asset('images/dg-img.png')" alt="">
      @endif
    </div>
    <div class="home-post-content">
      <header>
        @include('partials/entry-meta')
        <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
         <div class="sep"></div>
      </header>
      @if ( $firstPost == 0 )
        <div class="entry-summary">
          @php
          echo substr( get_the_excerpt(), 0, 232 ) @endphp
        </div>

        @php
        $btnTitle = 'read more';
        $btnLink = get_the_permalink();
        @endphp
        @include ( 'partials/buttons/primary-btn', [ $btnTitle, $btnLink ] )
      @endif
    </div>
  </div>
</article>
