@php
$titleBold      = 'Join Our';
$titleRegular   = 'Newsletter';
@endphp

<div class="dg-wc-login-form">
  @php
  woocommerce_login_form(  );
  @endphp
</div>

<footer class="content-info">
    @if ( ! is_404() )
    @include ( 'widgets/newsletter' )
    @endif
    <div class="container">
      <div class="widgets">
        @include ( 'widgets/footer-widgets' )
      </div>
      @include ( 'widgets/payment-methods' )
      <hr>
      <div class="grant-logos">
        <a target="_blank" href="https://eufunds.gov.mt/en/Pages/Home.aspx">
          <img src="@asset("images/grant-bottom.png")" alt="">
        </a>
      </div>
    </div>
</footer>

<div class="search-form-wrapper">
  <div class="wrapper">{{ get_search_form(  ) }}</div>
  <div class="btn-close">Close</div>
</div>
