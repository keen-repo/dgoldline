@php
$btnLink = get_permalink( wc_get_page_id( 'shop' ) );
$btnTitle = 'SHOP NOW';
@endphp
@if ( $shopmessage )
  <div class="site-wide-msg">
    <strong>{{ $shopmessage[ 'delivery_type' ] }}</strong>
    <span>{{ $shopmessage[ 'amount_text' ] }}</span>
    <a href="{{ $btnLink }}" class="shop-link">SHOP NOW <img src="@asset( 'images/icons/shop-now-arrow.png' )"></a>
  </div>
@endif
