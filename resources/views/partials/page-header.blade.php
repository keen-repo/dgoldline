@php
$page_for_posts = get_option( 'page_for_posts' );
@endphp

<div class="page-header">
  @if ( is_home(  ) )
  <h1>{!! get_the_title( $page_for_posts ) !!}</h1>
  @else
  <h1>{!! App::title() !!}</h1>
  @endif
</div>
