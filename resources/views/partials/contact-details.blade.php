@php
$flds    = get_fields( 'options' );
$cD      = $flds[ 'contact_numbers' ]; // get contact details
$numbers = $cD[ 'numbers' ];
$emails  = $cD[ 'emails' ];
@endphp

<address>{!! $cD[ 'address' ] !!}</address>

<div class="emails">
  <span>Email:</span>
  @foreach ( $emails as $email )
    <div class="email{{ $loop->iteration > 1 ? ' other-emails' : '' }}"><a href="mailto:{{ $email[ 'email' ] }}">{{ $email[ 'email' ] }}</a></div><br>
  @endforeach
</div>

<div class="numbers">
  @foreach ( $numbers as $number )
    @php
    $cc   = $number[ 'country_code' ];
    $num  = $number[ 'number' ];
    @endphp
    <div class="number"><a href="tel:{{ $cc ? '+' : '' }}{{ $cc }}{{ $number[ 'number' ] }}">{{ $cc ? '+' : '' }}{{ $cc }} {{ $num }}</a></div>
  @endforeach
</div>

@php $showSearch = false; @endphp
@include( 'partials.social-icons' )
