<div class="shop-menu-wrapper">
  <div class="shop-menu-logo">
    {!! App::getCustomLogo() !!}
  </div>
  <nav id="shop-menu" class="shop-menu nav-primary">
    @if (has_nav_menu('shop_navigation'))
      {!! wp_nav_menu(['theme_location' => 'shop_navigation', 'menu_class' => 'nav list-unstyled']) !!}
    @endif
  </nav>
  <div class="shop-menu-underlay"></div>
  <div class="btn-close">Close</div>
</div>
