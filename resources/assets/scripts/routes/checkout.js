export default {
  init() {
    // JavaScript to be fired on the about us page

    /*$( '.billing-info-name' ).text( $( '#billing_first_name' ).val(  ) );
    $( '.billing-info-email' ).text( $( '#billing_email' ).val(  ) );
    $( '.billing-info-phone' ).text( $( '#billing_phone' ).val(  ) );
    $( '.delivery-address-doornum' ).text( $( '#billing_door_number' ).val(  ) );
    $( '.delivery-address-1' ).text( $( '#billing_address_1' ).val(  ) );
    $( '.delivery-address-2' ).text( $( '#billing_address_2' ).val(  ) );
    $( '.delivery-address-city' ).text( $( '#billing_state' ).val(  ) );
    $( '.delivery-address-postcode' ).text( $( '#billing_postcode' ).val(  ) );
    $( '.receiver-info-name' ).text( $( '#shipping_first_name' ).val(  ) );*/

    let $shippingInputs = $( '.shipping_address input' );
    let shippingIDs = [  ];

    $shippingInputs.each( function( i ) {
      shippingIDs[ i ] = $( this ).attr( 'id' );
      $( '.' + shippingIDs[i] ).text( $( this ).val(  ) );

      $( '#' + shippingIDs[i] ).on( 'input', function(  ) {
        $( '.' + shippingIDs[i] ).text( $( this ).val(  ) );
      } );
    } );

    let $billingInputs = $( '.woocommerce-billing-fields input' );
    let billindIDs = [  ];

    $billingInputs.each( function( i ) {
      billindIDs[ i ] = $( this ).attr( 'id' );
      $( '.' + billindIDs[i] ).text( $( this ).val(  ) );

      $( '#' + billindIDs[i] ).on( 'input', function(  ) {
        $( '.' + billindIDs[i] ).text( $( this ).val(  ) );
      } );
    } );

    /**
     * NAVIGATION
     */

    $( '.dg-checkout-navigation .prev' ).on( 'click', function( e ) {
      e.preventDefault(  );
      $( '#customer_details' ).fadeIn(  );
      $( '#dg-order-review' ).fadeOut(  );

      $( 'html, body' ).animate( {
        scrollTop: $( 'form.checkout.woocommerce-checkout' ).offset(  ).top,
      }, 700 );

    } );

    $( '.dg-checkout-navigation .next' ).on( 'click', function( e ) {
      e.preventDefault(  );
      $( '#customer_details' ).fadeOut(  );
      $( '#dg-order-review' ).fadeIn(  );

      setTimeout( function(  ) {
        $( 'html, body' ).animate( {
          scrollTop: $( '#dg-order-review' ).offset(  ).top,
        }, 700 );
      }, 500 );

    } );
  },
};
