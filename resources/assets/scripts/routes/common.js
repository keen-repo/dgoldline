export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    let navDown = false;
    $( '.menu-toggle' ).on( 'click', () => {
      if ( navDown == false ) {
        $( '.main-navigation .nav' ).slideDown();
        navDown = true;
      } else if ( navDown == true ) {
        $( '.main-navigation .nav' ).slideUp();
        navDown = false;
      }
    });

    $( '.shop-menu .nav li a' ).one( 'click', function( e ) {
      let wW = $( window ).outerWidth(  );
      if ( wW < 600 ) {
        e.preventDefault(  );
        if ( $( this ).next(  ).hasClass( 'open' ) ) {
          $( this ).next(  ).slideUp(  ).removeClass( 'open' );
        } else {
          $( this ).next(  ).slideDown(  ).addClass( 'open' );
        }
      }
    } );

    $( '.shop-menu-open' ).on( 'click', function(  ) {
      $( '.shop-menu-wrapper' ).fadeIn(  );
    } );

    $( '.shop-menu-wrapper .btn-close' ).on( 'click', function(  ) {
      $( '.shop-menu-wrapper' ).fadeOut(  );
    } );

    $( '.social-icons .search-icon' ).on( 'click', function(  ) {
      $( '.search-form-wrapper' ).fadeIn(  );
      $( '.search-form-wrapper .btn-close' ).fadeIn(  );
    } );

    $( '.search-form-wrapper .btn-close' ).on( 'click', function(  ) {
      $( '.search-form-wrapper' ).fadeOut(  );
      $( '.search-form-wrapper .btn-close' ).fadeOut(  );
    } );

    /**
     * The cart popup
     */
    let cartPopupOpen = false;
    $( '.login-register-menu .last-item' ).on( 'click', function( e ) {
      e.preventDefault(  );
      if ( !cartPopupOpen ) {
        $( '.dg-cart-popup' ).fadeIn(  );
        cartPopupOpen = true;
      } else {
        $( '.dg-cart-popup' ).fadeOut(  );
        cartPopupOpen = false;
      }
    } );
    // The cart popup - END

    $( '.login-register-menu .logged-out' ).on( 'click', function( e ) {
      e.preventDefault(  );
      $( '.dg-wc-login-form' ).fadeIn(  );
    } );

    $( '.dg-wc-login-form .close' ).on( 'click', function( e ) {
      e.preventDefault(  );
      $( '.dg-wc-login-form' ).fadeOut(  );
    } );


    /**
     * Accordion setup
     */
    let questionWrapper = $( '.bulleted-option' );
    let question = $( '.bulleted-option-title' );
    let answers = $( '.bulleted-option-text' );
    question.on( 'click', openSibling );

    //let setTitle = $( '.links-sets .link-set-title' );
    //let setLinks = $( '.links-sets .link-set' );
    //setTitle.on( 'click', openSibling );

    function openSibling(  ) {
      // if the current answer is open
      if ( $( this ).next().hasClass( 'open' ) ) {
        // close it up, add class 'hide' and remove class 'open'
        $( this ).next().slideUp(  ).addClass( 'hide' ).removeClass( 'open' );
        $( this ).parent( ).removeClass( 'current' );
      } else {
        // otherwise, close all answers up, add class 'hide' and remove class 'open'
        //setLinks.slideUp(  ).addClass( 'hide' ).removeClass( 'open' );
        answers.slideUp(  ).addClass( 'hide' ).removeClass( 'open' );
        // then we open the answer to the question clicked
        $( this ).next( ).slideDown(  ).removeClass( 'hide' ).addClass( 'open' );
        questionWrapper.removeClass( 'current' );
        $( this ).parent(  ).addClass( 'current' );
      }
    }

    setTimeout( function( ) {
      doHeroImage(  );
      doTestimonialSlider(  );
      doHeroSlider(  );
      doShopCategorySlides(  );
      doYouMayAlsoWishSlider(  );
      simpleSliderSlides(  );
      removePreLoader(  );
    }, 200 );

    $( window ).on( 'resize', doHeroImage );
  },
};

function removePreLoader(  ) {
  $( '.dg-preloader-wrap' ).fadeOut(  );
}

function doHeroImage(  ) {
  let wW                  = $( window ).outerWidth(  );
  let wH                  = $( window ).outerHeight(  );
  let bannerHeight        = $( '.banner' ).outerHeight(  );
  let shopWrapperHeight   = $( '.shop-menu-wrapper' ).outerHeight( );
  let contentBoxHeight    = $( '.hero-slides .content-box' ).outerHeight();
  let heroSlider          = $( '.hero-slider-image' );
  let heroHeight          = wH - ( bannerHeight + shopWrapperHeight );

  if(contentBoxHeight > heroHeight - 200) {
    heroHeight = contentBoxHeight + 200;
  }

  if ( wW < 1024 ) {
    heroHeight = wH - bannerHeight;
  }

  heroSlider.css( 'height', heroHeight + 'px' );
}

function doHeroSlider(  ) {
  var heroSlider = {
    dots: true,
    arrows: true,
    //nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    //prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  $( '.hero-slides' ).slick( heroSlider );
}

function doTestimonialSlider(  ) {
  var testimonialSlider = {
    dots: false,
    arrows: true,
    nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  $( '.testimonials-slides' ).slick( testimonialSlider );
}

function doShopCategorySlides(  ) {
  var shopCategorySlides = {
    dots: false,
    arrows: true,
    nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  $( '.shop-category-slides' ).slick( shopCategorySlides );
}

function doYouMayAlsoWishSlider(  ) {
  var youMayAlsoWishSlider = {
    dots: false,
    arrows: true,
    nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  $( '.ymaw-products .products' ).slick( youMayAlsoWishSlider );
}

function simpleSliderSlides(  ) {
  var simpleSliderSlides = {
    centerMode: true,
    centerPadding: '390px', //260
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    nextArrow: '<div class="custom-slick-arrow custom-slick-next"></div>',
    prevArrow: '<div class="custom-slick-arrow custom-slick-prev"></div>',
    infinite: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1441,
        settings: {
          centerMode: true,
          centerPadding: '290px',
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 1381,
        settings: {
          centerMode: true,
          centerPadding: '20%',
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 1025,
        settings: {
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 1,
        },
      },
    ],
  };

  $( '.simple-slider-slides' ).slick( simpleSliderSlides );

  $( '.simple-slider-slides' ).on( 'afterChange', function(  ) {
    $( '.simple-slide' ).first( '.slick-active' ).addClass( 'twimpy' );
  } );
}
